import { Component, OnInit } from '@angular/core';


import { RestApiService } from "../shared/rest-api.service"; 

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css',]
})
export class HomeComponent implements OnInit {
  netVolume:any = 0;
  netValue:any=0;
  profit:any=0;
  today:any='';
  Traders: any = [];
  TradersSell:any=[];
  myArr: any = [];
  News:any=[];
  constructor(
    public restApi: RestApiService
    ) { 
    }

  ngOnInit(): void {
    this.loadTraders();
    this.today = new Date().toLocaleString("en-US", {timeZone: 'Asia/Kolkata'});

  }
  loadTraders() {
    //this.restApi.getNews().subscribe((data: {}) => {
      //this.News = data;
      //for (var i = 0; i < 7; i++) {
            //var str = JSON.stringify(this.News.articles[i]).split(":")[5];
            //str = str.substring(1, str.length - 15);

           // this.myArr.push(str);
  
//}
      //console.log(JSON.stringify(this.News.articles[0]));    
    //  console.log(this.myArr);    

  //})
    
    this.restApi.getTradersHome().subscribe((data: {}) => {
      this.Traders = data;
      var arrayLength = this.Traders.length;
      for (var i = 0; i < arrayLength; i++) {
            this.netValue+=(this.Traders[i].price*this.Traders[i].volume);
            this.netVolume+=this.Traders[i].volume;
}
  })

  this.restApi.getTradersHistory().subscribe((data: {}) => {
    this.TradersSell = data;
    var arrayLength = this.TradersSell.length;
    this.profit=this.netValue;
    for (var i = 0; i < arrayLength; i++) {
      if (this.TradersSell[i].buyOrSell=='sell'){
          this.profit-=(this.TradersSell[i].price*this.TradersSell[i].volume);
          console.log(this.profit);}}
}
)

          
  }


  deleteTrader(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteTrader(id).subscribe(data => {
        this.loadTraders()
      })
    }
  }
  
 

}
