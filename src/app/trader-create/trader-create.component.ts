import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-trader-create',
  templateUrl: './trader-create.component.html',
  styleUrls: ['./trader-create.component.css']
})
export class TraderCreateComponent implements OnInit {

  @Input() traderDetails = { id: 0,
    stockTicker: '',
    price: 0,
    volume: 0,
    buyOrSell: '',
    statusCode: 0 }

  constructor(
    public restApi: RestApiService, 
    public router: Router
  ) { }

  ngOnInit() { }

  addTrader() {
    this.restApi.createTrader(this.traderDetails).subscribe((data: {}) => {
      this.router.navigate(['/trader-list'])
    })
  }

}