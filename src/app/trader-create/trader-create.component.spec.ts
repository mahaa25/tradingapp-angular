import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TraderCreateComponent } from './trader-create.component';

describe('TraderCreateComponent', () => {
  let component: TraderCreateComponent;
  let fixture: ComponentFixture<TraderCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TraderCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TraderCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
