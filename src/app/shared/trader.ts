export class Trader {
    constructor(){
        this.stockTicker="";
        this.price=0;
        this.volume=0;
        this.buyOrSell="";
        this.statusCode=0;
        //this.date=new Date().toLocaleString("en-US", {timeZone: 'Asia/Kolkata'});
    }
    stockTicker: string;
    price: number;
    volume: number;
    buyOrSell: string;
    statusCode: number;
    //date: object;
}
