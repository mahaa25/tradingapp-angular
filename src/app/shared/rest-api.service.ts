import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Trader } from './trader';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  getTradersUser() {
    throw new Error('Method not implemented.');
  }
  UserURL = environment.UserUrl;
  AvailURL = environment.AvailUrl;
  HistoryURL = environment.HistoryUrl;
  OrderURL = environment.OrderUrl;
  newsURL =environment.newsUrl;

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'text/plain'
    })
  }  

  // HttpClient API get() method 
  getTraders(): Observable<Trader> {
    return this.http.get<Trader>(this.UserURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  getTradersHome(): Observable<Trader> {
    return this.http.get<Trader>(this.UserURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  getTradersOrder(): Observable<Trader> {
    return this.http.get<Trader>(this.AvailURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  getTradersHistory(): Observable<Trader> {
    return this.http.get<Trader>(this.HistoryURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  // HttpClient API get() method 
  getTrader(id:any): Observable<Trader> {
    return this.http.get<Trader>(this.UserURL + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  getNews(): Observable<Trader> {
    return this.http.get<Trader>(this.newsURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API post() method 
  createTrader(trader:Trader): Observable<Trader> {
    trader.statusCode=0;
    console.log(JSON.stringify(trader));
    console.log(this.OrderURL);
    return this.http.post<Trader>(this.OrderURL, JSON.stringify(trader), this.httpOptions)
    //.pipe(
      //retry(1),
      //catchError(this.handleError)
    //)
  } 

  // HttpClient API put() method
  updateTrader(id:number, trader:Trader): Observable<Trader> {
    return this.http.put<Trader>(this.UserURL, JSON.stringify(trader), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method 
  deleteTrader(id:number){
    return this.http.delete<Trader>(this.UserURL + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // Error handling 
  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
  
}
