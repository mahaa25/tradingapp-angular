import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmsellComponent } from './confirmsell.component';

describe('ConfirmsellComponent', () => {
  let component: ConfirmsellComponent;
  let fixture: ComponentFixture<ConfirmsellComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmsellComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmsellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
