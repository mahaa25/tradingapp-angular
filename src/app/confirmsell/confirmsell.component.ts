import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service"; 

@Component({
  selector: 'app-confirmsell',
  templateUrl: './confirmsell.component.html',
  styleUrls: ['./confirmsell.component.css']
})
export class ConfirmsellComponent implements OnInit {
  
  @Input() traderDetails = {
    stockTicker: '',
    price: 0,
    volume: 0,
    buyOrSell: '',
    statusCode: 0 }
    
  today:any='';
  public href: string = "";
  public a:any =[];
  
  public aprice:any = 0;
  constructor(private router: Router,
    public restApi: RestApiService

    ) {}

    Traders: any = [];

  ngOnInit(): void {
    this.a = this.router.url.split("/");
    this.href = this.a[this.a.length-1];
    this.href=this.href.replace("%20"," ");
    console.log("hi");
    console.log(this.href);
    this.loadTraders();
    this.today = new Date().toLocaleString("en-US", {timeZone: 'Asia/Kolkata'});

  }
  

  loadTraders() {
    this.restApi.getTradersHome().subscribe((data: {}) => {
        this.Traders = data;
        var arrayLength = this.Traders.length;
        for (var i = 0; i < arrayLength; i++) {
              if (this.Traders[i].stockTicker== this.href) {
                this.aprice=this.Traders[i].price;};
    //Do something
}
        console.log(this.Traders);
    })


  }

  addTrader() {
    this.restApi.createTrader(this.traderDetails).subscribe((data: {}) => {
      this.router.navigate(['/trading-history'])
    })
  }

}
