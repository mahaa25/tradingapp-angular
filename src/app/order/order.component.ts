import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  @Input() traderDetails = { id: 0,
    stockTicker: '',
    price: 0,
    volume: 0,
    buyOrSell: '',
    statusCode: 0 }

    Traders: any = [];
    TradersUser: any = [];
    today:any='';

  
  constructor(
    public restApi: RestApiService, 
    public router: Router
  ) { }
  
  ngOnInit(): void { 
    this.loadTraders();
    this.loadTraders1();
    this.today = new Date().toLocaleString("en-US", {timeZone: 'Asia/Kolkata'});

  }  

  loadTraders() {
    return this.restApi.getTradersOrder().subscribe((data: {}) => {
        this.Traders = data;
    })
  }
  loadTraders1() {
    return this.restApi.getTradersHome().subscribe((data: {}) => {
        this.TradersUser = data;
    })
  }

  
  addTrader() {
    this.restApi.createTrader(this.traderDetails).subscribe((data: {}) => {
      this.router.navigate(['/trading-history'])
    })
  }
}

