import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service"; 
@Component({
  selector: 'app-trading-history',
  templateUrl: './trading-history.component.html',
  styleUrls: ['./trading-history.component.css']
})
export class TradingHistoryComponent implements OnInit {
  Traders: any = [];
  today:any='';
  indic:any='yellow';

  constructor( 
    public restApi: RestApiService
    ) { 
     

    }

  ngOnInit(): void {
    this.loadTraders();
    this.today = new Date().toLocaleString("en-US", {timeZone: 'Asia/Kolkata'});
  }

  loadTraders() {
    return this.restApi.getTradersHistory().subscribe((data: {}) => {
        this.Traders = data;
        var arrayLength = this.Traders.length;
        for (var i = 0; i < arrayLength; i++) {
          if (this.Traders[i].volume== 0) {
            this.Traders[i].volume="Order Placed";};
            if (this.Traders[i].volume== 1) {
              this.Traders[i].volume="Processing";};
              if (this.Traders[i].volume== 2) {
                this.Traders[i].volume="Success";};
                if (this.Traders[i].volume== 3) {
                  this.Traders[i].volume="Failure";};
          
//Do something
}
    })
  }

}
