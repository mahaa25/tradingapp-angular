import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service"; 

@Component({
  selector: 'app-trader-list',
  templateUrl: './trader-list.component.html',
  styleUrls: ['./trader-list.component.css']
})
export class TraderListComponent implements OnInit {

  Traders: any = [];
  constructor( 
    public restApi: RestApiService
    ) { }

  ngOnInit(): void {
    this.loadTraders()
  }

  loadTraders() {
    return this.restApi.getTraders().subscribe((data: {}) => {
        this.Traders = data;
    })
  }

  deleteTrader(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteTrader(id).subscribe(data => {
        this.loadTraders()
      })
    }
  }  

}
