import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmbuyComponent } from './confirmbuy.component';

describe('ConfirmbuyComponent', () => {
  let component: ConfirmbuyComponent;
  let fixture: ComponentFixture<ConfirmbuyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmbuyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmbuyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
