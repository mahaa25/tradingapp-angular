import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service"; 

@Component({
  selector: 'app-confirmbuy',
  templateUrl: './confirmbuy.component.html',
  styleUrls: ['./confirmbuy.component.css']
})
export class ConfirmbuyComponent implements OnInit {
  
  @Input() traderDetails = {
    stockTicker: 'default',
    price: 0,
    volume: 0,
    buyOrSell: '',
    statusCode: 0 }

  public href: string = "";
  public a:any =[];
  today:any='';

  public aprice:any = 0;
  constructor(private router: Router,
    public restApi: RestApiService

    ) {}

    Traders: any = [];

  ngOnInit(): void {
    this.a = this.router.url.split("/");
    this.href = this.a[this.a.length-1];
    this.href=this.href.replace("%20"," ");
    console.log("hi");
    console.log(this.href);
    this.loadTraders();
    this.today = new Date().toLocaleString("en-US", {timeZone: 'Asia/Kolkata'});

  }
  

  loadTraders() {
    this.restApi.getTradersOrder().subscribe((data: {}) => {
        this.Traders = data;
        var arrayLength = this.Traders.length;
        for (var i = 0; i < arrayLength; i++) {
              if (this.Traders[i].stockTicker== this.href) {
                this.aprice=this.Traders[i].price;};
    //Do something
}
        console.log(this.Traders);
    })


  }

  addTrader() {
    console.log(this.traderDetails);
    this.restApi.createTrader(this.traderDetails).subscribe((data: {}) => {
      this.router.navigate(['/trading-history'])
    })
  }

}
